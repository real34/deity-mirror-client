import React, { Component } from 'react';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { load } from '../../redux/modules/entity';
import Product from './containers/Product';
import Category from './containers/Category';

class Entity extends Component {
  componentDidMount() {
    if (this.props.location) {
      this.props.load(this.props.location.pathname);
    }
  }

  componentWillReceiveProps(nextProps) {
    const nextLocation = nextProps.location;

    if (!this.props.location || nextLocation.pathname !== this.props.location.pathname) {
      this.props.load(nextLocation.pathname);
    }
  }

  render () {
    const { entity } = this.props;

    if (entity.loading) {
      return (
        <p>Loading</p>
      )
    }


    if (entity.data) {
      const response = entity.data.data;
      const type = response && response.type;

      if (type === 'product') {
        return <Product {...response} />
      } else if (type === 'category') {
        return <Category {...response} />
      }
    }

    return <div/>
  }
}

const mapStateToProps = ({ entity }) => ({ entity });

const mapDispatchToProps = (dispatch) => {
  return {
    load: bindActionCreators(load, dispatch),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Entity)
