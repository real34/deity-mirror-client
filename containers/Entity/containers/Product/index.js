import React, { Component } from 'react';
import styles from './styles.scss';

class Product extends Component {
  render () {
    const { name, image, price, extension_attributes: { thumbnail_resized_url } } = this.props;

    return (
      <div className={styles.product}>
        <div className="row">
          <div className="col-sm-6">
            <img src={thumbnail_resized_url} alt={name} />
          </div>
          <div className="col-sm-6">
            <h1>{name}</h1>
            <strong>{price}</strong>
          </div>
        </div>
      </div>
    )
  }
}

export default Product;
