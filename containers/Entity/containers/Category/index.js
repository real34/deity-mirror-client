import React, { Component } from 'react';
import styles from './styles.scss';

class Category extends Component {
  getDescription() {
    return this.props.custom_attributes && this.props.custom_attributes.description;
  }

  render () {
    const { name } = this.props;
    const description = this.getDescription();

    return (
      <div className={styles.category}>
        <h1>{name}</h1>
        {description && <p dangerouslySetInnerHTML={{ __html: description }} />}
      </div>
    )
  }
}

export default Category;
