import styles from './styles.scss';

export default () => (
  <div className={styles.text}>
    <p>
    This project shows how it feels to work with real Deity software before it is publicly available.

    Built using <a href="https://learnnextjs.com/">Next.js</a> with its great documentation and a wrapper for all webpack/babel complexity.

    It uses external node api gateway and port some of the components of the original client.
    </p>
  </div>
)
