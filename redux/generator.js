/**
 * @param {String} module | main reducer name
 * @param {String} type | reducer property
 * @return {Array} | promise states
 */

export const getPromiseTypes = (module, type) => ['REQUEST', 'SUCCESS', 'FAIL']
  .map(item => `${module}/${type}_${item}`);

/**
 * Generate reducer function.
 * @param {String} module | main reducer name
 * @param {String} type | reducer property
 * @param {Object} options | data properties
 * @return {Function} reducer
 */
export function generateReducer(module, type, options = {}) {
  const [request, success, fail] = getPromiseTypes(module, type);

  const initialState = {
    loaded: false,
    loading: false,
    data: {
      ...options
    }
  };

  return function reducer(state = initialState, action = {}) {
    const { result, error } = action;

    switch (action.type) {
      case request:
        return { ...state, loading: true, loaded: false };
      case success:
        return { ...state, loading: false, loaded: true, data: result };
      case fail:
        return { ...state, loading: false, loaded: true, error };
      default:
        return state;
    }
  };
}

/**
 * Generate simple list reducer with items, filters and pagination.
 * @param {String} module | main reducer name
 * @param {String} type | reducer property
 * @param {Object} customOptions | data properties
 * @return {Function} generated reducer
 */
export function generateListReducer(module, type, customOptions = {}) {
  const options = {
    items: [],
    filters: [],
    pagination: {},
    ...customOptions
  };

  return generateReducer(module, type, options);
}

/**
 * Generate "isLoaded" function to check whether required module has loaded its data
 * @param {Object} globalState redux state
 * @param {String} module redux module name
 * @param {String|null} subModule sub module
 * @return {Boolean} if state is loaded
 */
export function generateIsLoaded(globalState, module, subModule = null) {
  let { [module]: moduleState } = globalState;

  if (subModule) {
    moduleState = moduleState && moduleState[subModule];
  }

  return moduleState && moduleState.loaded;
}
