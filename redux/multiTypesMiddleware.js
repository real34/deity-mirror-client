export default function clientMiddleware(client) {
  return ({ dispatch, getState }) => next => action => {
    if (typeof action === 'function') {
      return action(dispatch, getState);
    }

    const { promise, types, ...rest } = action; // eslint-disable-line no-redeclare
    if (!promise) {
      return next(action);
    }

    const [REQUEST, SUCCESS, FAILURE] = types;

    try {
      next({ ...rest, type: REQUEST });

      return promise(client)
        .then(result => next({ ...rest, result, type: SUCCESS }))
        .catch(error => {
          console.error(error);
          next({ ...rest, error, type: FAILURE });
          return error;
        });
    } catch (error) {
      console.error(error);
      throw new Error('Redux error');
    }
  };
}
