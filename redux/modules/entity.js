import { generateListReducer, getPromiseTypes } from '../generator';

const module = 'entity';
const type = 'LOAD';

export default generateListReducer(module, type);

export function load(url) {
  const params = { url };

  return {
    types: getPromiseTypes(module, type),
    promise: client => client.get('/url', { params })
  };
}
