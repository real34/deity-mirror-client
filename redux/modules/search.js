const SEARCH_SUCCESS = 'search/RESULTS_SUCCESS';
const SEARCH_FAIL = 'search/RESULTS_FAIL';
const SEARCH = 'search/RESULTS';

const initialState = {
  loading: false,
  loaded: false,
  data: {},
  query: '',
  items: []
};

export default function reducer(state = initialState, action = {}) {
  const { result, error, query } = action;

  switch (action.type) {
    case SEARCH: {
      return { ...state, loading: true, loaded: false, query };
    }
    case SEARCH_SUCCESS: {
      return { ...state, loading: false, loaded: true, data: result };
    }
    case SEARCH_FAIL: {
      return { ...state, error, loading: false, loaded: true };
    }
    default:
      return state;
  }
}

export function load(query) {
  return {
    type: SEARCH,
    types: [SEARCH, SEARCH_SUCCESS, SEARCH_FAIL],
    query,
    promise: client => client.get('/search', {
      params: {
        currency: 'EUR',
        query
      }
    })
  };
}
