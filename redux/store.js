import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';
import multiTypesMiddleware from './multiTypesMiddleware';
import thunkMiddleware from 'redux-thunk';
import search from './modules/search';
import entity from './modules/entity';
import logger from 'redux-logger'

const client = axios.create({
  baseURL:'http://localhost:3000/api',
  responseType: 'json'
});

export const initStore = (initialState = {}) => {
  return createStore(
    combineReducers({ entity, search }),
    initialState,
    composeWithDevTools(
      applyMiddleware(
        logger,
        multiTypesMiddleware(client),
        axiosMiddleware(client),
        thunkMiddleware
      )
    )
  )
};
